#include <stdlib.h>
#include <string.h>
#include "pq.h"

#define PQ_SWAP(a, b) do { \
	typeof(a) __swap_tmp = a; \
	a = b; \
	b = __swap_tmp; \
} while(0)

static inline ssize_t find_parent(ssize_t n)
{
	if(n == 1)
		return -1;
	else
		return (n / 2); // floor
}

static inline size_t youngest_child(size_t n)
{
	return n * 2;
}

static void bubble_up(pqueue *pq, size_t n)
{
	pq_keydata *p1, *p2;

	if(find_parent(n) == -1)
		return; // root of the heap
	else
	{
		p1 = &pq->q[find_parent(n)];
		p2 = &pq->q[n];
		if(p1->prio > p2->prio)
		{
			PQ_SWAP(*p1, *p2);
			bubble_up(pq, find_parent(n));
		}
	}
}

static void bubble_down(pqueue *pq, size_t n)
{
	size_t mindex = n; // minimum index
	size_t c = youngest_child(n);

	// Check the youngest child and the next leaf
	if (c <= pq->elements)
	{
		if(pq->q[mindex].prio > pq->q[c].prio)
			mindex = c;
	}

	if (c + 1 <= pq->elements)
	{
		if(pq->q[mindex].prio > pq->q[c + 1].prio)
			mindex = c + 1;
	}

	if(mindex != n)
	{
		PQ_SWAP(pq->q[n], pq->q[mindex]);
		bubble_down(pq, mindex);
	}
}

void pqueue_init(pqueue *pq)
{
	pq->q = calloc(sizeof(pq_keydata), PQ_INIT_SIZE + sizeof(pq_keydata));
	pq->size = PQ_INIT_SIZE;
	pq->elements = 0;
}

void pqueue_deinit(pqueue *pq)
{
	free(pq->q);
    pq->q = NULL;
    pq->size = 0;
    pq->elements = 0;
}

pqueue* pqueue_insert(pqueue* list, int priority, void *data)
{
    pq_keydata *tmp;
	pq_keydata *kd;

	if(list->elements >= list->size - 1)
	{
        tmp = calloc(sizeof(pq_keydata), list->size * 2 + 1);
        if (tmp == NULL)
            return NULL;

        memcpy(tmp, list->q, (list->size + 1)* sizeof(pq_keydata));
        free(list->q);
        list->q = tmp;
        list->size *= 2;
	}

	list->elements += 1;
	kd = &list->q[list->elements];
	kd->data = data;
	kd->prio = priority;

	bubble_up(list, list->elements);

	return list;
}

void* pqueue_front(pqueue* list)
{
	pq_keydata ret = { -1, NULL };

	if(list->elements >= 1)
	{
		ret = list->q[1];

		list->q[1] = list->q[list->elements];
		list->elements -= 1;
		bubble_down(list, 1);
	}

	return ret.data;
}

int pqueue_peek(pqueue* list)
{
	if(list->elements >= 1)
		return list->q[1].prio;
	else
		return -1;
}

