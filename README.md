# Data Structures in C

C should be easier to program in. Currently, libc has very little in the way of advanced data structures. This small project aims to make that problem slightly less of an issue.

## Implemented Data Structures

This is a list of data structures currently implemented.

### Priority Queue

A min heap implementation of a [priority queue](http://en.wikipedia.org/wiki/Priority_queue).

### Splay Tree

A [splay tree](http://en.wikipedia.org/wiki/Splay_tree) implementation with either integer key comparison or arbitrary data with supplied comparison function.

### Radix Trie

A [radix trie](http://en.wikipedia.org/wiki/Radix_trie) implementation that operates on bytes - which means arbitrary data can be the prefix - and supports multiple data pointers per key.

