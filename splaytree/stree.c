#include <stdbool.h>
#include <stdlib.h>
#include "stree.h"

#define STREE_IS_LEFT_CHILD(n) \
    (n->parent == NULL ?       \
     (false) :                 \
     (n == n->parent->left))

#define STREE_IS_RIGHT_CHILD(n) \
    (n->parent == NULL ?        \
     (false) :                  \
     (n == n->parent->right))

#define STREE_IS_ROOT(n) (n->parent == NULL)

#define SSWAP(a, b) do { \
	typeof(a) __swap_tmp = a; \
	a = b; \
	b = __swap_tmp; \
} while(0)

// http://lcm.csa.iisc.ernet.in/dsa/node93.html
static void rotateright(streen *n)
{
    streen *gp = n->parent;
    streen *small = n->left;
    streen *t2 = small->right;

    if(!STREE_IS_ROOT(n))
    {
        if(STREE_IS_LEFT_CHILD(n))
            gp->left = small;
        else
            gp->right = small;
    }

    n->parent = small;
    n->left = t2;
    small->right = n;
    small->parent = gp;
    n->parent = small;

    if(t2 != NULL)
        t2->parent = n;
}

static void rotateleft(streen *n)
{
    streen *gp = n->parent;
    streen *large = n->right;
    streen *t2 = large->left;

    if(!STREE_IS_ROOT(n))
    {
        if(STREE_IS_LEFT_CHILD(n))
            gp->left = large;
        else
            gp->right = large;
    }

    n->parent = large;
    n->right = t2;
    large->left = n;
    large->parent = gp;
    n->parent = large;

    if(t2 != NULL)
        t2->parent = n;
}

static void splay(streen *nptr, stree *tree)
{
    streen *parent;
    if(nptr == NULL)
        return;

    parent = nptr->parent;

    if(parent == NULL)
    {
        // This is the root
        tree->root = nptr;
    }
    // Zig, splay is complete
    else if(STREE_IS_ROOT(parent))
    {
        if(STREE_IS_LEFT_CHILD(nptr))
            rotateright(parent);
        else
            rotateleft(parent);

        tree->root = nptr; // now this is the root
    }
    else
    {
        // Zig-zig right
        if(STREE_IS_LEFT_CHILD(nptr) && STREE_IS_LEFT_CHILD(parent))
        {
            rotateright(parent->parent);
            rotateright(parent);
        }
        // Zig-zig left
        else if(STREE_IS_RIGHT_CHILD(nptr) && STREE_IS_RIGHT_CHILD(parent))
        {
            rotateleft(parent->parent);
            rotateleft(parent);
        }
        // Zig-zag
        else
        {
            if(STREE_IS_LEFT_CHILD(nptr))
            {
                rotateright(parent);
                rotateleft (nptr->parent);
            }
            else
            {
                rotateleft (parent);
                rotateright(nptr->parent);
            }
        }
        splay(nptr, tree);
    }
}

void stree_init(stree *s, stree_cmp_func f, skey_t key, void *data)
{
#ifdef STREE_CMP_INT
    s->f = 0;
#else
    s->f = f;
#endif
    s->root = calloc(sizeof(streen), 1);
    s->root->key    = key;
    s->root->data   = data;
    s->root->left   = NULL;
    s->root->right  = NULL;
    s->root->parent = NULL;

    s->num = 1;
}

void stree_insert(stree *s, skey_t key, void *data)
{
    bool left = false;
    streen *ptr = s->root;
    streen *prev = NULL;
    streen *curr = NULL;
    
    while(ptr != NULL)
    {
#ifdef STREE_CMP_INT
        if(ptr->key < key)
#else
        if(s->f(ptr->key, key) < 0)
#endif
        {
            prev = ptr;
            ptr = prev->right;
            left = false;
        }
        else
        {
            prev = ptr;
            ptr = prev->left;
            left = true;
        }
    }

    if(left)
    {
        prev->left = calloc(sizeof(streen), 1);
        prev->left->key  = key;
        prev->left->data = data;
        prev->left->parent = prev;
        curr = prev->left;
    }
    else
    {
        prev->right = calloc(sizeof(streen), 1);
        prev->right->key  = key;
        prev->right->data = data;
        prev->right->parent = prev;
        curr = prev->right;
    }

    s->num++;
    splay(curr, s); // splay to the top
}

void *stree_find(stree *s, skey_t key)
{
    streen *ptr = s->root;
    streen *prev = NULL;
    void *d;
    
    while(ptr != NULL)
    {
#ifdef STREE_CMP_INT
        if((int)ptr->key == (int)key)
#else
        if(s->f(ptr->key, key) == 0)
#endif
        {
            prev = ptr;
            d = prev->data;
            splay(prev, s);
            return d;
        }
#ifdef STREE_CMP_INT
        else if((int)ptr->key < (int)key)
#else
        else if(s->f(ptr->key, key) < 0)
#endif
        {
            prev = ptr;
            ptr = prev->right;
        }
        else
        {
            prev = ptr;
            ptr = prev->left;
        }
    }

    return NULL;
}

static void streen_free(streen *node)
{
    if(node->left != NULL)
        streen_free(node->left);
    if(node->right != NULL)
        streen_free(node->right);
    free(node);
}

void stree_free(stree *s)
{
    streen_free(s->root);
    s->num = 0;
    s->root = NULL;
}

