#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <stdbool.h>
#include <assert.h>
#include "radix.h"

#define CHECK_ALLOC(ptr) if ((ptr) == NULL) return RADIX_NONE

static inline int radix_insert_node(struct radix_node *parent, u8 *key,
        int key_len, void *data, bool free_ptr) {
    parent->nodes = realloc(parent->nodes, sizeof(struct radix_node) * (parent->num_nodes + 1));
    CHECK_ALLOC(parent->nodes);
    parent->nodes[parent->num_nodes] = calloc(1, sizeof(struct radix_node));

    struct radix_node *new_node = parent->nodes[parent->num_nodes++];
    CHECK_ALLOC(new_node);

    new_node->data = malloc(key_len);
    CHECK_ALLOC(new_node->data);

    memcpy(new_node->data, key, key_len);
    new_node->len = key_len;
    new_node->nodes = NULL;
    new_node->num_nodes = 0;
    new_node->ptr = malloc(sizeof(void*));
    CHECK_ALLOC(new_node->ptr);

    new_node->ptr_sz = 1;
    new_node->num_ptrs = 1;
    new_node->ptr[0] = data;
    new_node->free_ptr = free_ptr;

    return RADIX_OK;
}


static inline int is_prefix(struct radix_node *n, u8 *key, int key_len) {
    const int min_len = MIN(n->len, key_len);
    return min_len == n->len && !memcmp(n->data, key, min_len);
}

static inline int is_partial_prefix(struct radix_node *n, u8 *key, int key_len) {
    const int min_len = MIN(n->len, key_len);
    int i = 0;

    for (; i < min_len; i++) {
        if (n->data[i] != key[i])
            break;
    }
    return i;
}

static inline int cleanup_tombstones(struct radix_node *n, int index) {
    struct radix_node **new_nodes = calloc(n->num_nodes - 1, sizeof(struct radix_node*));
    CHECK_ALLOC(new_nodes);
    int j = 0, new_j = index;
    memcpy(new_nodes, n->nodes, index * sizeof(struct radix_node*));
    for (j = index; j < n->num_nodes; j++) {
        if (n->nodes[j]->len != -1)
            new_nodes[new_j++] = n->nodes[j];
        else
            free(n->nodes[j]);
    }
    n->num_nodes = new_j;
    free(n->nodes);
    n->nodes = new_nodes;
    return RADIX_OK;
}

static int gather(struct radix_node *n, void **result, int max_results) {
    int results = 0;
    int i = 0;

    for (; i < n->num_nodes; i++) {
        if (n->nodes[i]->len == -1) {
            if (RADIX_OK == cleanup_tombstones(n, i))
                i--;
            continue;
        }

        results += gather(n->nodes[i], result + results, max_results - results);
    }

    // If there's room, include this node's data
    for (i = 0; i < (max_results - results) && i < n->num_ptrs; i++) {
        result[results + i] = n->ptr[i];
    }

    return results + i;
}

static int delete(struct radix_node *n, void *data) {
    int results = 0;
    int i = 0;
    bool delete_children = false;
    const int starting_ptrs = n->num_ptrs;

    // If we were given only one specific data pointer to delete
    // delete all occurences of it
    if (data != NULL) {
        for (i = 0; i < n->num_ptrs; i++) {
            if (n->ptr[i] == data) {
                if (i + 1 < n->num_ptrs)
                    memmove(&n->ptr[i], &n->ptr[i + 1], (n->num_ptrs - i + 1) * sizeof(void*));
                n->num_ptrs--;
                delete_children = true;
            }
        }
    }

    // If we've deleted all of our data pointers when one was specified,
    // automatically delete all children
    void *child_ptr = delete_children && n->num_ptrs == 0 ? NULL : data;
    if (n->num_nodes > 0) {
        struct radix_node **new_children = calloc(n->num_nodes, sizeof(struct radix_node*));
        CHECK_ALLOC(new_children);

        int new_nodes = 0;
        for (; i < n->num_nodes; i++) {
            if (n->nodes[i]->len == -1)
                continue;

            results += delete(n->nodes[i], child_ptr);
            if (n->nodes[i]->len == -1)
                free(n->nodes[i]);
            else 
                new_children[new_nodes++] = n->nodes[i];
        }

        free(n->nodes);
        n->nodes = new_children;
        n->num_nodes = new_nodes;
    }


    // If we weren't given a data pointer delete this node
    // If we were, and we have no data pointers left, delete this node
    if (data == NULL || 
            (data != NULL && starting_ptrs > 0 && n->num_ptrs == 0)) {
        free(n->nodes);
        free(n->data);

        if (n->free_ptr) {
            for (i = 0; i < n->num_ptrs; i++)
                free(n->ptr[i]);
        }

        free(n->ptr);
        // Use a tombstone
        n->len = -1;
        return results + 1;
    }
    return results;
}

static struct radix_node* search(struct radix_node *root, u8 *key, int key_len, int *found) {
    int found_len = 0;
    struct radix_node *n = root;

    while (n != NULL && n->num_nodes > 0 && found_len < key_len) {
        int i = 0;

        bool found = false;
        for (; i < n->num_nodes; i++) {
            if (n->nodes[i]->len == -1) {
                // this was deleted, clean all tombstoned nodes
                if (RADIX_OK == cleanup_tombstones(n, i))
                    i--;
                continue;
            }

            int plen = 0;
            if (is_prefix(n->nodes[i], key + found_len, key_len - found_len)) {
                found = true;
                break;
            } else if ((plen = is_partial_prefix(n->nodes[i], key + found_len, 
                        key_len - found_len)) > 0) {
                // Found a halfway match
                found_len += plen;
                n = n->nodes[i];
                goto end;
            }
        }

        if (found) {
            n = n->nodes[i];
            found_len += n->len;
        } else {
            break;
        }
    }

end:
    if (found != NULL)
        *found = found_len;

    return n;
}

void radix_init(struct radix_node *root) {
    root->data = NULL;
    root->len = 0;
    root->nodes = NULL;
    root->num_nodes = 0;
    root->free_ptr = false;
    root->ptr = NULL;
}

int radix_lookup(struct radix_node *root, u8 *key, int key_len, 
        void **result, int max_results) {
    int found = 0;
    struct radix_node *n = search(root, key, key_len, &found);

    // assemble results
    if (found >= key_len)
        return gather(n, result, max_results);
    else
        return 0;
}

int radix_delete(struct radix_node *root, u8 *key, int key_len, void *data) {
    int found = 0;
    int deleted = 0;
    struct radix_node *n = search(root, key, key_len, &found);

    if (found >= key_len || key_len == 0) {
        deleted = delete(n, data);
    }

    return deleted > 0 ? RADIX_OK : RADIX_NONE;
}

int radix_insert(struct radix_node *root, u8 *key, int key_len, void *data, bool free_ptr) {
    int found_len = 0;
    struct radix_node *n = root;

    assert(key != NULL);
    assert(key_len > 0); 

    while (n != NULL && n->num_nodes > 0 && found_len < key_len) {
        int i = 0;

        bool found = false;
        for (; i < n->num_nodes; i++) {
            if (n->nodes[i]->len == -1) {
                // this was deleted, clean all tombstoned nodes
                if (RADIX_OK == cleanup_tombstones(n, i))
                    i--;
                continue;
            }

            int plen = 0;
            if (is_prefix(n->nodes[i], key + found_len, key_len - found_len)) {
                found = true;
                break;
            } else if ((plen = is_partial_prefix(n->nodes[i], key + found_len, 
                            key_len - found_len)) > 0) {
                // Need to split
                struct radix_node *split = malloc(sizeof(struct radix_node));
                CHECK_ALLOC(split);

                split->len = plen;
                split->data = malloc(plen);
                CHECK_ALLOC(split->data);

                memcpy(split->data, n->nodes[i]->data, plen);

                split->nodes = malloc(2 * sizeof(struct radix_node*));
                CHECK_ALLOC(split->nodes);

                split->nodes[0] = n->nodes[i];
                split->num_nodes = 1;
                split->ptr = malloc(sizeof(void*));
                CHECK_ALLOC(split->ptr);
                split->ptr_sz = 1;
                split->num_ptrs = 0;
                split->free_ptr = false;

                assert(n->nodes[i]->len > plen);
                memmove(n->nodes[i]->data, n->nodes[i]->data + plen, 
                        n->nodes[i]->len - plen);
                n->nodes[i]->len -= plen;
                n->nodes[i] = split;

                assert(key_len - found_len - plen > 0);
                return radix_insert_node(split, key + found_len + plen, 
                        key_len - found_len - plen, data, free_ptr);
            }
        }

        if (found) {
            n = n->nodes[i];
            found_len += n->len;
        } else {
            break;
        }
    }

    if (found_len == key_len) { 
        // Append data
        if (n->ptr_sz <= n->num_ptrs) {
            n->ptr = realloc(n->ptr, n->ptr_sz);
            CHECK_ALLOC(n->ptr);
            n->ptr_sz *= 2;
        }
        n->ptr[n->num_ptrs++] = data;
        return RADIX_OK;
    }

    assert(key_len - found_len > 0);
    return radix_insert_node(n, key + found_len, key_len - found_len, data, free_ptr);
}


